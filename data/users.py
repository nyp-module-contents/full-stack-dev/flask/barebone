import shelve
# from data import open_database
from data     import TableRow, Table
from typing   import List, Dict

import uuid
import hashlib


class User(TableRow):
	"""
	This represents a single row in the user table
	"""
	# _private_key = object()
	def __init__(self, uid: str = None, username: str = "", password: str = "", role: str = "user"):
		"""
		:param key: private constructor blocker
		:param uid: universally unique identifier
		:param username: username
		:param password: password hashed
		"""
		# if key is not User._private_key:
		# 	raise PermissionError("This structure cannot be constructed publicly. Please use via parent Table")
		super().__init__(uid)
		try:
			self._username = username
			self._password = hashlib.sha256(password.encode()).hexdigest()
			self._role     = role
		except Exception as exception:
			raise exception

	@property
	def username(self):
		"""
		:return: The specified username
		"""
		return self._username

	@property
	def password(self):
		"""
		:return: The hashed password
		"""
		return self._password

	@property
	def role(self):
		"""
		:return: The role of the user account
		"""
		return self._role

	def password_change(self, password_old: str, password_new: str) -> bool:
		"""
		Updates the password of this user. The old password must match in order to succeed.
		:param password_old: str The old password in plain string
		:param password_new: str The new password in plain string
		:return: success or failure
		"""
		password_old = str(hashlib.sha256(password_old.encode('utf-8')).digest())

		if self._password == password_old:
			self._password = str(hashlib.sha256(password_new.encode('utf-8')).digest())
			return True
		else:
			return False

	def to_json(self) -> dict:
		return {
			**(super().to_json()),
			"username": self._username,
			"role":     self._role
		}


class TableUsers(Table[User]):
	def __init__(self, handle: shelve.Shelf):
		super().__init__(handle)

	@property
	def name(self) -> str:
		return "Users"

	def select(self, query: dict) -> List[User]:
		contents = self.list()

		if "where" in query:
			if "uuid" in query["where"]:
				contents = list(filter(lambda x: x.uuid == uuid.UUID(query["where"]["uuid"]), contents))
			if "username" in query["where"]:
				contents = list(filter(lambda x: x.username == query["where"]["username"], contents))
			if "password" in query["where"]:
				contents = list(filter(lambda x: x.password == query["where"]["password"], contents))

		if "order" in query:
			if "username" in query["order"]:
				contents = sorted(contents, key = lambda x: x.username)

		return contents
