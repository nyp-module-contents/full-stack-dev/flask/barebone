# Bare bones Example

This example shows how to have a basic flask web server setup with 2 pages `home` and `about`.

## Configuration

### OS Environment

Ensure you have python installed in system path. For Linux/Unix/Mac users, you will use `python3` instead of `python`

```shell
python -v
```

If you didn't, add your installation folder path to the PATH in system environment settings.

For windows:

https://www.architectryan.com/2018/08/31/how-to-change-environment-variables-on-windows-10/

Use `Edit Environment variables for your account` if you do not own the system.

### Create virtual environment

Run the following command in your terminal:

```shell
python -m venv venv
```

### Install dependencies

Run the following command in your terminal:

```shell
pip install -r requirements.txt
```

## Run the server

### Activate Virtual Environment

Activate the virtual environment before running the server. You should see that your terminal has a `(venv)` next to your prefix. For example:

```shell
(venv) PS D:/Modules/dev-full-stack-flask/barebone>
```

On some instances, it might be automatic depending on your IDE. In that case, you may skip this step.

#### Windows Users

Run the following command in your terminal:

```shell
./venv/Scripts/activate
```

If you encounter errors about powershell execution policies. Run the following command as a administrator:

```powershell
Set-ExecutionPolicy Unrestricted
```

#### Linux/Unix/Mac

Run the following command in your terminal:

```shell
source ./venv/bin/activate
```

### Execute the Web Server

Run the following command in your terminal once you have activated your virtual environment:

```shell
python -m server
```
