from flask import Blueprint, request
from flask import render_template


endpoint = Blueprint("base", __name__)

@endpoint.route("/")
def page_home():
	return render_template(
		"home.html",
		pageTitle="Home")


@endpoint.route("/about")
def page_about():
	return render_template(
		"about.html",
		pageTitle="About Us")


@endpoint.route("/catalog")
def page_catalog():
	from data import Database
	from data.products import Product, TableProduct

	pageIdx  = int(request.args.get('page')     or 0)
	pageSize = int(request.args.get('pageSize') or 8)


	#	WHERE implies => Filter conditions
	#	Limit implies => Paging
	#	Sort  implies order


	with Database() as db:
		table = db.table(TableProduct)
		return render_template(
			"catalog.html",
			products=table.select({
				"limit": {
					"offset": pageIdx * pageSize,
					"size"  : pageSize
				}
			})
		)