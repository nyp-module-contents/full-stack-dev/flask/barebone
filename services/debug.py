"""
This service module is strictly reserved for debugging routes.
This should not be include in production. :)
"""
from flask import Blueprint, Flask
from flask import render_template, url_for, redirect, current_app

endpoint = Blueprint("debug", __name__)

@endpoint.route("/")
def page_base():
	return redirect("sitemap")

@endpoint.route("/sitemap")
def page_sitemap():
	"""
	This route displays the entire site map of the server. Use this to debug errors of
	`Cannot GET/POST...etc  XXXXXX`
	:return:
	"""
	links:  list  = []
	server: Flask = current_app
	rule:   Flask.url_rule_class = None

	for rule in server.url_map.iter_rules():
		defaults = rule.defaults  or {}
		args     = rule.arguments or {}
		#	Skip things that cannot be reached
		if (len(defaults) < len(args)):
			continue

		url = url_for(rule.endpoint, **defaults)

		for method in rule.methods:
			if method == "HEAD" or method == "OPTIONS":
				continue
			links.append({
				"method": method,
				"url":    url
			})

	return render_template("debug/sitemap.html", routes=links)