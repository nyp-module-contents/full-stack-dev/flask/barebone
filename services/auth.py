from flask import Blueprint, current_app
from flask import wrappers, request, make_response
from flask import render_template, redirect
from flask import session
from data  import Database

endpoint = Blueprint("auth", __name__)


@endpoint.route("/register")
def page_register():
	from services.forms.auth import FormRegister
	form_content = FormRegister(request.form)

	return render_template("auth/register.html", form=form_content)


@endpoint.route("/login")
def page_login():
	from services.forms.auth import FormLogin
	form_content = FormLogin(request.form)

	if request.method == "POST":
		pass
	else:
		if "user" in session:
			return redirect("/")
		else:
			return render_template("auth/login.html", form=form_content)


@endpoint.route("/logout")
def page_logout():
	if "user.uuid" in session:
		del session["user.uuid"]
	if "user.role" in session:
		del session["user.role"]
	if "user.username" in session:
		del session["user.username"]
	return redirect("/")


@endpoint.route("/register", methods=["POST"])
def api_register():
	"""
	This function will register a new user
	:return:
	"""
	from services.forms.auth import FormRegister
	form_content = FormRegister(request.form)

	from hashlib   import sha256
	from data.users import TableUsers, User

	if not form_content.validate():
		return render_template("auth/register.html", form=form_content)
	else:
		try:
			username = form_content.username.data
			password = form_content.password.data

			if not username or not password:
				raise ValueError("Missing required parameters")
		except Exception as exception:
			current_app.logger.error(f"(400) Bad request: {exception}")
			return wrappers.Response(status = 400)

	try:
		with Database() as db:
			table  = db.table(TableUsers)
			result = table.select(query = {
				"where": {
					"username": username
				}
			})

			#	If not found in database, create and insert
			if (len(result) == 0):
				table.insert(User(username=username, password=password))
				table.commit()
				current_app.logger.debug(f"Signed in as {username}")
				return redirect("/")
			#	Else signin and assign to session
			else:
				return render_template("auth/register.html", form = form_content)

	except Exception as exception:
		current_app.logger.error(f"Failed to register as {username}: {exception}")
		return wrappers.Response(status = 500)


@endpoint.route("/login", methods=["POST"])
def api_login():
	"""
	This function will login the specified user and assign to session
	:return:
	"""
	from services.forms.auth import FormLogin
	form_content = FormLogin(request.form)

	from hashlib   import sha256
	from data.users import TableUsers, User

	if not form_content.validate():
		return render_template("auth/login.html", form=form_content)

	try:
		username = request.form["username"]
		password = request.form["password"]

		if not username or not password:
			raise ValueError("Missing required parameters")
	except Exception as exception:
		current_app.logger.error(f"(400) Bad request: {exception}")
		return wrappers.Response(status = 400)

	try:
		with Database() as db:
			table  = db.table(TableUsers)
			result = table.select(query = {
				"where": {
					"username": username,
					"password": sha256(password.encode()).hexdigest()
				}
			})

			#	If not found in database, 401 unauthorized
			if (len(result) == 0):
				current_app.logger.debug(f"Signed in as {username}")
				return wrappers.Response(status = 401)
			#	Else signin and assign to session
			else:
				session["user.uuid"]     = result[0].uuid
				session["user.username"] = result[0].username
				session["user.role"]     = result[0].role
				current_app.logger.debug(f"Signed in as {username}")
				return redirect("/")
	except Exception as exception:
		current_app.logger.error(f"Failed to sign in as {username}: {exception}")
		return wrappers.Response(status = 500)
