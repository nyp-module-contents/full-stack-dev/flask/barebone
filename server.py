import os
import secrets
from   flask import Flask

from flask_wtf.csrf import CSRFProtect


# Create the webserver object and assign to variable name application
application = Flask(__name__,
					template_folder = os.path.join(os.getcwd(), "templates"),  #	Path of the template directory
					static_folder   = os.path.join(os.getcwd(), "public"),     #	Path of the public data directory
					static_url_path ="/public")

# For secure sessions
# application.config['SECRET_KEY'] = secrets.token_urlsafe(16)
application.config['SECRET_KEY'] = "N.A"

# Enable CSRF token for forms
csrf = CSRFProtect()
csrf.init_app(application)

# TODO: Do not include the debug endpoint for production or live builds.
from services.debug  import endpoint as EP_Debug
application.register_blueprint(EP_Debug,  url_prefix="/debug")
# TODO: Register all your subroutes here, import the blueprint as a different name following conventions
from services.common import endpoint as EP_Common
from services.auth   import endpoint as EP_Auth
from services.admin  import endpoint as EP_Admin
application.register_blueprint(EP_Common, url_prefix="/")
application.register_blueprint(EP_Auth,   url_prefix="/auth")
application.register_blueprint(EP_Admin,  url_prefix="/admin")

from werkzeug.exceptions import HTTPException

@application.errorhandler(HTTPException)
def error_generic(error: HTTPException):
	from flask import render_template, current_app
	current_app.logger.error(f"{error}")
	return render_template("error/error.html", error=error)

@application.route("/dynamic/<path:path>", methods=["GET"])
def serve_dynamic_content(path: str):
	"""
	Serve any file path from "instance" folder
	as http://localhost:3000/dynamic/<YourPath>
	"""
	from flask import send_from_directory, current_app, wrappers
	path_file = os.path.join(current_app.instance_path, path)

	if not os.path.exists(path_file):
		return wrappers.Response(status=404)
	else:
		return send_from_directory(current_app.instance_path, path)

# This is your main entry point
# In other common languages such as "C/C++/Java" you will need to be very clear where entry point is
# This serves as practice and a point of reference where your program starts

# As you know you can import code from other files, this section ensures that it doesn't run when its just being imported
if __name__ == "__main__":
	# TODO: Place your initializations here
	import data
	import data.users
	import uuid

	with data.Database() as db:
		table = db.table(data.users.TableUsers)
		table.clear()
		table.insert(data.users.User(username = "admin", password = "P@ssw0rd", role = "admin", uid = str(uuid.UUID('00000000-0000-0000-0000-000000000000'))))
		for i in range(20):
			table.insert(data.users.User(username = f"user{i:02d}", password = "P@ssw0rd", role = "user"))
		table.commit()

		application.logger.debug(f"Users : {table.list()}")

	# Make dynamic content directory, put all your file uploads here
	if not os.path.exists(application.instance_path):
		os.makedirs(application.instance_path, exist_ok=True)

	# Run the web server at the specified address and port,
	# the debug flag sets to auto refresh otherwise your changes won't appear
	# Doesn't guarantee anyway but gives you convenience
	application.run(host = "localhost", port = 3000, debug = True)